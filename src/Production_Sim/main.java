package Production_Sim;

public class main {
	
	/*
	 * Démarre le programme
	 */
	public static void main(String[] args) {
		
		Interface_p Interface_p = new Interface_p();
		Modele_p Modele_p = new Modele_p();
		
		new Controleur_p(Interface_p, Modele_p);

		Interface_p.frame.setVisible(true);
		
	}

}