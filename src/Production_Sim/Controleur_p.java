package Production_Sim;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Controleur_p {
	
	private Interface_p v;
	private Modele_p m;
	
	private int recette_33_A, recette_33_B, recette_33_C;
	private int recette_77_A, recette_77_B, recette_77_C;
	
	
	public Controleur_p(Interface_p v, Modele_p m) {
		
		this.v = v;
		this.m = m;
		
		try {
			this.recette_33_A = m.rechercheBD("Recettes", "qteProdA", "nomTable", "Tables 33");
			this.recette_33_B = m.rechercheBD("Recettes", "qteProdB", "nomTable", "Tables 33");
			this.recette_33_C = m.rechercheBD("Recettes", "qteProdC", "nomTable", "Tables 33");
			this.recette_77_A = m.rechercheBD("Recettes", "qteProdA", "nomTable", "Tables 77");
			this.recette_77_B = m.rechercheBD("Recettes", "qteProdB", "nomTable", "Tables 77");
			this.recette_77_C = m.rechercheBD("Recettes", "qteProdC", "nomTable", "Tables 77");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		initialisation();
		
		v.addProductionListener(new productionListener());
		v.addArretListener(new arretListener());
		v.addTimerListener(new productionListener());
		v.addCommanderListener(new commanderListener());
	}
	
	/*
	 * Initialise les données de l'interface en ayant chercher dans la BD
	 */
	public void initialisation() {
		try {
			int stockA = m.rechercheBD("Stocks", "qteStock", "typeStock", "stockA");
			v.setStockA(stockA);
			v.setLblStockA(stockA);
			int stockB = m.rechercheBD("Stocks", "qteStock", "typeStock", "stockB");
			v.setStockB(stockB);
			v.setLblStockB(stockB);
			int stockC = m.rechercheBD("Stocks", "qteStock", "typeStock", "stockC");
			v.setStockC(stockC);
			v.setLblStockC(stockC);
			int stockTables33 = m.rechercheBD("Stocks", "qteStock", "typeStock", "tables 33");
			v.setTable33Complete(stockTables33);
   			v.setStockTable33(stockTables33);
			int stockTables77 = m.rechercheBD("Stocks", "qteStock", "typeStock", "tables 77");
			v.setTable77Complete(stockTables77);
   			v.setStockTable77(stockTables77);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Production en cours. Soustrait les quantités de stock pour chaque table, une fois 
	 * le progresProduit à 100, une table est completé et ajouté à l'interface et la BD.
	 * Les valeurs des stocks et produits sont mis à jour en temps réel. 
	 */
	public void modeProduction() {
		
		int stockA;
		int stockB;
		int stockC;
		int progresProduit;
		int tableComplete;
		int quantite;
           	
   		stockA = v.getStockA();
   		stockB = v.getStockB();
   		stockC = v.getStockC();
   		progresProduit = v.getProgresProduit();

	   	progresProduit++;
	   	
	   	if (progresProduit == 1 && stockA > 0) {
	   		quantite = recette_33_A;
	   		if (stockA >= quantite) {
	 	   		stockA = m.utiliserStock(stockA, quantite);
	   		}
	   	} else if (progresProduit == 33 && stockB > 0) {
	   		if (v.rdbtnTables33.isSelected()) {
	   			quantite = recette_33_B;
	   		} else {
	   			quantite = recette_77_B;
	   		}
	   		if (stockB >= quantite) {
	   			stockB = m.utiliserStock(stockB, quantite);
	   		}
	   	} else if (progresProduit == 66 && stockC > 0) {
	   		if (v.rdbtnTables33.isSelected()) {
	   			quantite = recette_33_C;
	   		} else {
	   			quantite = recette_77_C;
	   		}
	   		if (stockC >= quantite) {
	   			stockC = m.utiliserStock(stockC, quantite);
	   		}
	   	} else if (progresProduit == 100 &&(stockA != 0 || stockB != 0 || stockC != 0)) {	
	   		if (v.rdbtnTables33.isSelected()) {
	   			tableComplete = v.getTable33Complete();
	   			tableComplete++;
	   			v.setTable33Complete(tableComplete);
	   			v.setStockTable33(tableComplete);
	   		} else {	   			
	   			tableComplete = v.getTable77Complete();
	   			tableComplete++;
	   			v.setTable77Complete(tableComplete);
	   			v.setStockTable77(tableComplete);
	   		}
	   		progresProduit = -1;
			try {
				m.updateBD("Stocks", "qteStock", "typeStock", v.getTable33Complete(), "tables 33");
				m.updateBD("Stocks", "qteStock", "typeStock", v.getTable77Complete(), "tables 77");
			} catch (SQLException e) {
				e.printStackTrace();
			}
	   	}
	   	
	   	v.setProgresProduit(progresProduit);   	
	   	v.setStockA(stockA);
	   	v.setLblStockA(stockA); 	
	   	v.setStockB(stockB);
	   	v.setLblStockB(stockB);	
	   	v.setStockC(stockC);
	   	v.setLblStockC(stockC);

	}
	
	/*
	 * Gère la production et les tables à produire. Vérifie quelle table produire, que le nombre de tables
	 * produites ne dépassent pas deux semaines de ventes et qu'il y ait moins d'une semaine de ventes afin
	 * de redémarrer la production.
	 * note: les ventes par semaines sont une moyenne des ventes cherchés dans la BD
	 */
	public void tablesManagement() {
		
		int VSemaines = 150;
		int V2Semaines = 300; 
		int totalTables = v.getTable33Complete()+v.getTable77Complete();
		
		try {
			VSemaines = m.requeteBD("SELECT AVG(qteTotale) FROM Ventes;", "AVG(qteTotale)")*5;
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		V2Semaines = VSemaines * 2;
		
		if (v.chckbxAutoProd.isSelected()) {
			if (v.getTable33Complete()-v.getTable77Complete() > 10) {
				v.rdbtnTables77.setSelected(true);
			} else if (v.getTable77Complete()-v.getTable33Complete() > 10) {
				v.rdbtnTables33.setSelected(true);
			}
		} else {
			if (v.getTable33Complete() >= 150) {
				v.rdbtnTables77.setSelected(true);
			} else if (v.getTable77Complete() >= 150) {
				v.rdbtnTables33.setSelected(true);
			}
		}
		if (totalTables >= V2Semaines-1 || totalTables == 300) {
			v.lblArretDeProduction.setVisible(true);
		}
		if (totalTables < VSemaines) {
			v.lblArretDeProduction.setVisible(false);
		}
		if (!(v.lblArretDeProduction.isVisible())) {
			modeProduction();
		}
	}
	
	/*
	 * Applique les ventes sur le stock de tables produites. Met à jour l'interface et la BD à chaque vente.
	 * note: Génère un log dans la BD avec la journée, les stocks et les ventes.
	 */
	public void venteManagement() {
		
		int i = 0;
		int vente33 = m.venteProduit();
		int table33 = v.getTable33Complete();
		
		while (vente33 > table33) {
			vente33 = m.venteProduit();
			if (i == 3) {
				vente33 = table33;
			}
			i++;
		}
		
		v.setTable33Complete(table33-vente33);
		v.setStockTable33(table33-vente33);
		try {
			m.updateBD("Stocks", "qteStock", "typeStock", table33-vente33, "tables 33");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		int vente77 = m.venteProduit();
		int table77 = v.getTable77Complete();
		
		while (vente77 > table77) {
			vente77 = m.venteProduit();
			if (i == 3) {
				vente77 = table77;
			}
			i++;
		}
		
		v.setTable77Complete(table77-vente77);
		v.setStockTable77(table77-vente77);
		try {
			m.updateBD("Stocks", "qteStock", "typeStock", table77-vente77, "tables 77");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			int resultat = m.requeteBD("SELECT AVG(qteTotale) FROM Ventes;", "AVG(qteTotale)");
			m.updateBD("VentesStats", "qteVentes", "VentesPar", resultat, "Jour");
			m.updateBD("VentesStats", "qteVentes", "VentesPar", resultat*5, "Semaine");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String date = m.concatDate(v.getLblJour(), v.getLblSemaine());
		try {
			m.insereBD(date, vente33, vente77, vente33+vente77);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			m.insereLogBD(date, v.getStockA(), v.getStockB(), v.getStockC(), table33, table77, vente33, vente77, vente33+vente77);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * Gère le temps écoulé pour faire certaines actions à une intervalle spécfique de temps.
	 * note: met à jour le temps sur l'interface. Le temps continue à s'écouler en arrêt de production.
	 */
	public void tempsManagement() {
		
		int heure = v.getLblHeure();
		int jour = v.getLblJour();
		int semaine = v.getLblSemaine();
		int ticker = v.getTicker();
		
		if (ticker < 400) {
			ticker++;
		} else {
			ticker = 0;
			if (heure < 8) {
				heure ++;
			} else {
				heure = 1;
				if (jour < 5) {
					jour++;
				} else {
					jour = 1;
					semaine++;
					v.setLblSemaine(semaine);
				}
				v.setLblJour(jour);
				venteManagement();
			}
			v.setLblHeure(heure);
		}
		v.setTicker(ticker);
	}
	
	/*
	 * Organise les commandes de stocks pour produire les tables. 
	 * Dépendamment de si les commandes automatiques sont séléctionnés, 
	 * la fonction de commande automatique peut être appelée.
	 * Met à jour les stocks de la BD. 
	 */
	public void stockManagement() {
		
		int stockA = v.getStockA();
   		int stockB = v.getStockB();
   		int stockC = v.getStockC();
		
	   	if (v.rdbtnTables33.isSelected()) {
		   	if (stockA < recette_33_A || stockB < recette_33_B || stockC < recette_33_C) {
				v.timer.stop();
				v.setBtnProduction(false);
				v.setBtnCommander(true);
				if (stockA < recette_33_A) {
					v.setBtnCommanderColorCYAN();
				} else if (stockB < recette_33_B) {
					v.setBtnCommanderColorORANGE();
				} else if (stockC < recette_33_C) {
					v.setBtnCommanderColorGREEN();
				}
		   	} else {
		   		v.setBtnProduction(true);
		   	}
	   	} else {
	   		if (stockA < recette_77_A || stockB < recette_77_B || stockC < recette_77_C) {
	   			v.timer.stop();
	   			v.setBtnProduction(false);
	   			v.setBtnCommander(true);
	   			if (stockA < recette_77_A) {
					v.setBtnCommanderColorCYAN();
				} else if (stockB < recette_77_B) {
					v.setBtnCommanderColorORANGE();
				} else if (stockC < recette_77_C) {
					v.setBtnCommanderColorGREEN();
				}
	   		} else {
	   			v.setBtnProduction(true);
	   		}
	   	}
	   	if (v.getAutoCommande() == true) {
	   		commandeStock();
			v.timer.start();
	   	}
	   	try {
			m.updateBD("Stocks", "qteStock", "typeStock", stockA, "stockA");
			m.updateBD("Stocks", "qteStock", "typeStock", stockB, "stockB");
			m.updateBD("Stocks", "qteStock", "typeStock", stockC, "stockC");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Créé une commande pour les stocks manquants.
	 */
	public void commandeStock() {

		if (v.getStockA() < recette_33_A) {
			int quantiteCommande = m.creerCommande(recette_33_A);
			v.setStockTotalA(quantiteCommande);  // 2 semaines = 1200
			v.setStockA(quantiteCommande);
		} else if (v.getStockB() < recette_77_B) {
			int quantiteCommande = m.creerCommande(recette_77_B);
			v.setStockTotalB(quantiteCommande);
			v.setStockB(quantiteCommande);	// 2 semaines = 750
		} else if (v.getStockC() < recette_77_C) {
			int quantiteCommande = m.creerCommande(recette_77_C);
			v.setStockTotalC(quantiteCommande);
			v.setStockC(quantiteCommande);	// 2 semaines = 800
		}
		v.setBtnProduction(true);
		v.setBtnCommanderColorDefault();
		v.setBtnCommander(false);
	}
	
	/*
	 * Met à jour les stocks en cherchant dans la BD le stock spécifié.
	 */
	public int updateStock(String stock) {
		int newStock = Integer.parseInt(stock);
		try {
			newStock = m.rechercheBD("Stocks", "qteStock", "typeStock", stock);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return newStock;
	}
	
	/*
	 * Listener pour le boutton Production.
	 * Démarre le timer qui démarre la production.
	 */
	class productionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			v.timer.start();
			v.timer.setDelay(10/4);
			
			tempsManagement();
			stockManagement();
			tablesManagement();
		}
	}
	
	/*
	 * Listener pour le boutton Arrêt.
	 * Arrête le timer et donc la production.
	 */
	class arretListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			v.timer.stop();
		}
	}
	
	/*
	 * Fait une commande pour le stock manquant.
	 * note: une commande se fait automatiquement quand Auto-Commande est selectionné,
	 * ce bouton est désactivé quand Auto-Commande est selectioné.
	 */
	class commanderListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			commandeStock();
		}
	}
}
