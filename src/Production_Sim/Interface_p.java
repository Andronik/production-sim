package Production_Sim;

import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.Font;
import java.awt.Cursor;
import javax.swing.DebugGraphics;
import java.awt.SystemColor;
import javax.swing.JCheckBox;
import javax.swing.UIManager;

public class Interface_p {

	JFrame frame;
	JButton btnProduction;
	JButton btnArret;
	JButton btnCommander;
	JProgressBar stock_A;
	JProgressBar stock_B;
	JProgressBar stock_C;
	JProgressBar progresProduit;
	JProgressBar table33complete;
	JProgressBar table77complete;
	JRadioButton rdbtnTables33;
	JRadioButton rdbtnTables77;
	JCheckBox chckbxAutoCommande;
	JCheckBox chckbxAutoProd;
	JLabel lblArretDeProduction;
	Timer timer;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	JLabel lblstocka;
	JLabel lblstockb;
	JLabel lblstockc;
	private JLabel lblPattes;
	private JLabel lblDessus;
	private JLabel lblPeinture;
	private JLabel lblStockA;
	private JLabel lblStockB;
	private JLabel lblStockC;
	private JLabel lblStockT33;
	private JLabel lblStockT77;
	private JLabel lblProductionTablesStables;
	private JLabel lbltemps;
	private JLabel lblJour;
	private JLabel lblHeure;
	private JLabel lblticker;
	private JLabel lblSemaine;

	/**
	 * Create the application.
	 */
	public Interface_p() {
		initialize();
	}

	/**
	 * Initialize the contents of th148, 14e frame.
	 */
	public void initialize() {
		
		frame = new JFrame();
		frame.setResizable(false);
		frame.getContentPane().setEnabled(false);
		frame.setBounds(100, 100, 960, 630);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		timer = new Timer(0, null);
		
		stock_A = new JProgressBar();
		stock_A.setMaximum(6000);
		stock_A.setForeground(Color.CYAN);
		stock_A.setValue(6000);
		stock_A.setOrientation(SwingConstants.VERTICAL);
		stock_A.setBackground(Color.LIGHT_GRAY);
		stock_A.setBounds(285, 129, 100, 305);
		frame.getContentPane().add(stock_A);
		
		stock_B = new JProgressBar();
		stock_B.setMaximum(160); 	//160
		stock_B.setForeground(Color.ORANGE);
		stock_B.setValue(160); 		// 160
		stock_B.setBackground(Color.LIGHT_GRAY);
		stock_B.setOrientation(SwingConstants.VERTICAL);
		stock_B.setBounds(425, 129, 100, 305);
		frame.getContentPane().add(stock_B);
		
		stock_C = new JProgressBar();
		stock_C.setMaximum(300);	//300
		stock_C.setForeground(Color.GREEN);
		stock_C.setValue(300);		//300
		stock_C.setBackground(Color.LIGHT_GRAY);
		stock_C.setOrientation(SwingConstants.VERTICAL);
		stock_C.setBounds(570, 129, 100, 305);
		frame.getContentPane().add(stock_C);
		
		btnProduction = new JButton("Production");
		btnProduction.setEnabled(true);
		btnProduction.setBounds(60, 150, 164, 44);
		frame.getContentPane().add(btnProduction);
		
		btnArret = new JButton("Arrêt");
		btnArret.setBounds(60, 206, 164, 44);
		frame.getContentPane().add(btnArret);
		
		progresProduit = new JProgressBar();
		progresProduit.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		progresProduit.setValue(0);
		progresProduit.setBackground(Color.LIGHT_GRAY);
		progresProduit.setForeground(Color.BLUE);
		progresProduit.setBounds(285, 509, 385, 30);
		frame.getContentPane().add(progresProduit);
		
		table33complete = new JProgressBar();
		table33complete.setForeground(SystemColor.desktop);
		table33complete.setMaximum(150);
		table33complete.setOrientation(SwingConstants.VERTICAL);
		table33complete.setBounds(734, 181, 72, 299);
		frame.getContentPane().add(table33complete);
		
		table77complete = new JProgressBar();
		table77complete.setForeground(SystemColor.desktop);
		table77complete.setOrientation(SwingConstants.VERTICAL);
		table77complete.setMaximum(150);
		table77complete.setBounds(824, 181, 72, 299);
		frame.getContentPane().add(table77complete);
		
		rdbtnTables33 = new JRadioButton("Tables 33");
		rdbtnTables33.setHorizontalAlignment(SwingConstants.LEFT);
		rdbtnTables33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		rdbtnTables33.setFont(new Font("Dialog", Font.BOLD, 12));
		buttonGroup.add(rdbtnTables33);
		rdbtnTables33.setSelected(true);
		rdbtnTables33.setBounds(70, 285, 154, 23);
		frame.getContentPane().add(rdbtnTables33);
		
		rdbtnTables77 = new JRadioButton("Tables 77");
		rdbtnTables77.setHorizontalAlignment(SwingConstants.LEFT);
		rdbtnTables77.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		rdbtnTables77.setFont(new Font("Dialog", Font.BOLD, 12));
		buttonGroup.add(rdbtnTables77);
		rdbtnTables77.setBounds(70, 312, 154, 23);
		frame.getContentPane().add(rdbtnTables77);
		
		lblstocka = new JLabel("Stock A");
		lblstocka.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblstocka.setHorizontalAlignment(SwingConstants.CENTER);
		lblstocka.setBounds(285, 443, 100, 15);
		frame.getContentPane().add(lblstocka);
		
		lblstockb = new JLabel("Stock B");
		lblstockb.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblstockb.setHorizontalAlignment(SwingConstants.CENTER);
		lblstockb.setBounds(435, 443, 90, 15);
		frame.getContentPane().add(lblstockb);
		
		lblstockc = new JLabel("Stock C");
		lblstockc.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblstockc.setHorizontalAlignment(SwingConstants.CENTER);
		lblstockc.setBounds(580, 443, 90, 15);
		frame.getContentPane().add(lblstockc);
		
		lblPattes = new JLabel("Pattes");
		lblPattes.setHorizontalAlignment(SwingConstants.CENTER);
		lblPattes.setBounds(285, 102, 100, 15);
		frame.getContentPane().add(lblPattes);
		
		lblDessus = new JLabel("Dessus");
		lblDessus.setHorizontalAlignment(SwingConstants.CENTER);
		lblDessus.setBounds(425, 102, 100, 15);
		frame.getContentPane().add(lblDessus);
		
		lblPeinture = new JLabel("Peinture");
		lblPeinture.setHorizontalAlignment(SwingConstants.CENTER);
		lblPeinture.setBounds(570, 102, 104, 15);
		frame.getContentPane().add(lblPeinture);
		
		lblStockA = new JLabel("Stock A");
		lblStockA.setFont(new Font("Dialog", Font.BOLD, 14));
		lblStockA.setHorizontalAlignment(SwingConstants.CENTER);
		lblStockA.setBounds(285, 81, 100, 15);
		frame.getContentPane().add(lblStockA);
		
		lblStockB = new JLabel("Stock B");
		lblStockB.setFont(new Font("Dialog", Font.BOLD, 14));
		lblStockB.setHorizontalAlignment(SwingConstants.CENTER);
		lblStockB.setBounds(425, 81, 100, 15);
		frame.getContentPane().add(lblStockB);
		
		lblStockC = new JLabel("Stock C");
		lblStockC.setFont(new Font("Dialog", Font.BOLD, 14));
		lblStockC.setHorizontalAlignment(SwingConstants.CENTER);
		lblStockC.setBounds(570, 81, 100, 15);
		frame.getContentPane().add(lblStockC);
		
		btnCommander = new JButton("Commander");
		btnCommander.setBackground(Color.WHITE);
		btnCommander.setEnabled(false);
		btnCommander.setBounds(60, 404, 164, 44);
		frame.getContentPane().add(btnCommander);
		
		JLabel lblTables = new JLabel("Tables 33");
		lblTables.setHorizontalAlignment(SwingConstants.CENTER);
		lblTables.setBounds(734, 163, 72, 15);
		frame.getContentPane().add(lblTables);
		
		JLabel lblTables_1 = new JLabel("Tables 77");
		lblTables_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblTables_1.setBounds(824, 163, 72, 15);
		frame.getContentPane().add(lblTables_1);
		
		chckbxAutoCommande = new JCheckBox("Auto-commande");
		chckbxAutoCommande.setSelected(true);
		chckbxAutoCommande.setHorizontalAlignment(SwingConstants.LEFT);
		chckbxAutoCommande.setFont(new Font("Dialog", Font.BOLD, 12));
		chckbxAutoCommande.setBounds(70, 456, 149, 23);
		frame.getContentPane().add(chckbxAutoCommande);
		
		lblStockT33 = new JLabel("0");
		lblStockT33.setHorizontalAlignment(SwingConstants.CENTER);
		lblStockT33.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblStockT33.setBounds(734, 492, 72, 15);
		frame.getContentPane().add(lblStockT33);
		
		lblStockT77 = new JLabel("0");
		lblStockT77.setHorizontalAlignment(SwingConstants.CENTER);
		lblStockT77.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblStockT77.setBounds(824, 492, 72, 15);
		frame.getContentPane().add(lblStockT77);
		
		JLabel lblStockTables = new JLabel("Stock Tables");
		lblStockTables.setHorizontalAlignment(SwingConstants.CENTER);
		lblStockTables.setFont(new Font("Dialog", Font.BOLD, 14));
		lblStockTables.setBounds(740, 139, 156, 15);
		frame.getContentPane().add(lblStockTables);
		
		lblProductionTablesStables = new JLabel("Production Tables Stables & Fiables");
		lblProductionTablesStables.setHorizontalAlignment(SwingConstants.CENTER);
		lblProductionTablesStables.setFont(new Font("Dialog", Font.BOLD, 18));
		lblProductionTablesStables.setBounds(12, 15, 936, 30);
		frame.getContentPane().add(lblProductionTablesStables);
		
		lblHeure = new JLabel("1");
		lblHeure.setBounds(336, 482, 30, 15);
		frame.getContentPane().add(lblHeure);
		
		lblJour = new JLabel("1");
		lblJour.setBounds(390, 482, 30, 15);
		frame.getContentPane().add(lblJour);
		
		lblSemaine = new JLabel("1");
		lblSemaine.setBounds(478, 482, 30, 15);
		frame.getContentPane().add(lblSemaine);
		
		lbltemps = new JLabel("Heure:     Jour:     Semaine:");
		lbltemps.setBounds(285, 482, 209, 15);
		frame.getContentPane().add(lbltemps);
		
		lblticker = new JLabel("0");
		lblticker.setForeground(UIManager.getColor("Button.background"));
		lblticker.setBounds(271, 482, 30, 15);
		frame.getContentPane().add(lblticker);
		
		chckbxAutoProd = new JCheckBox("Auto-production");
		chckbxAutoProd.setSelected(true);
		chckbxAutoProd.setBounds(70, 258, 154, 23);
		frame.getContentPane().add(chckbxAutoProd);
		
		lblArretDeProduction = new JLabel("Arrêt de production");
		lblArretDeProduction.setVisible(false);
		lblArretDeProduction.setHorizontalAlignment(SwingConstants.CENTER);
		lblArretDeProduction.setForeground(Color.RED);
		lblArretDeProduction.setBounds(506, 482, 168, 15);
		frame.getContentPane().add(lblArretDeProduction);
		
	}
	
	public int getTicker() {
		return Integer.parseInt(lblticker.getText());
	}
	
	public void setTicker(int ticker) {
		lblticker.setText(Integer.toString(ticker));
	}
	
	public int getLblHeure() {
		return Integer.parseInt(lblHeure.getText());
	}

	public void setLblHeure(int heure) {
		lblHeure.setText(Integer.toString(heure));
	}
	
	public int getLblJour() {
		return Integer.parseInt(lblJour.getText());
	}

	public void setLblJour(int jour) {
		lblJour.setText(Integer.toString(jour));
	}
	
	public int getLblSemaine() {
		return Integer.parseInt(lblSemaine.getText());
	}

	public void setLblSemaine(int semaine) {
		lblSemaine.setText(Integer.toString(semaine));
	}

	public int getStockTable33() {
		return Integer.parseInt(lblStockT33.getText());
	}
	
	public void setStockTable33(int stockTable33) {
		lblStockT33.setText(Integer.toString(stockTable33));
	}
	
	public int getStockTable77() {
		return Integer.parseInt(lblStockT77.getText());
	}
	
	public void setStockTable77(int stockTable77) {
		lblStockT77.setText(Integer.toString(stockTable77));
	}
	
	public boolean getAutoCommande() {
		return chckbxAutoCommande.isSelected();
	}
	
	public void setLblStockA(int stockA) {
		lblstocka.setText(Integer.toString(stockA));
	}
	
	public void setLblStockB(int stockB) {
		lblstockb.setText(Integer.toString(stockB));
	}
	public void setLblStockC(int stockC) {
		lblstockc.setText(Integer.toString(stockC));
	}
	
	public void setBtnProduction(boolean bool) {
		this.btnProduction.setEnabled(bool);
	}
	
	public void setBtnCommander(boolean bool) {
		this.btnCommander.setEnabled(bool);
	}
	
	public void setBtnCommanderColorDefault() {
		this.btnCommander.setBackground(Color.WHITE);
	}
	
	public void setBtnCommanderColorCYAN() {
		this.btnCommander.setBackground(Color.CYAN);
	}
	
	public void setBtnCommanderColorORANGE() {
		this.btnCommander.setBackground(Color.ORANGE);
	}
	
	public void setBtnCommanderColorGREEN() {
		this.btnCommander.setBackground(Color.GREEN);
	}

	public int getStockA() {
		return stock_A.getValue();
	}
	
	public void setStockA(int quantite_A) {
		stock_A.setValue(quantite_A);
	}
	
	public void setStockTotalA(int total_A) {
		stock_A.setMaximum(total_A);
	}
	
	public int getStockB() {
		return stock_B.getValue();
	}
	
	public void setStockB(int quantite_B) {
		stock_B.setValue(quantite_B);
	}
	
	public void setStockTotalB(int total_B) {
		stock_B.setMaximum(total_B);
	}
	
	public int getStockC() {
		return stock_C.getValue();
	}
	
	public void setStockTotalC(int total_C) {
		stock_C.setMaximum(total_C);
	}
	
	public void setStockC(int quantite_C) {
		stock_C.setValue(quantite_C);
	}
	
	public int getProgresProduit() {
		return progresProduit.getValue();
	}
	
	public void setProgresProduit(int quantiteP) {
		progresProduit.setValue(quantiteP);
	}
	
	public int getTable33Complete() {
		return table33complete.getValue();
	}

	public void setTable33Complete(int quantiteT) {
		table33complete.setValue(quantiteT);
	}
	
	public int getTable77Complete() {
		return table77complete.getValue();
	}

	public void setTable77Complete(int quantiteT) {
		table77complete.setValue(quantiteT);
	}
	
	
	void addProductionListener(ActionListener listenProductionBtn) {
		btnProduction.addActionListener(listenProductionBtn);
	}
	
	void addArretListener(ActionListener listenArretBtn) {
		btnArret.addActionListener(listenArretBtn);
	}
	
	void addTimerListener(ActionListener listenProductionBtn) {
		timer.addActionListener(listenProductionBtn);
	}
	
	void addCommanderListener(ActionListener listenCommanderBtn) {
		btnCommander.addActionListener(listenCommanderBtn);
	}
}
