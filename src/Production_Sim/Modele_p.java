package Production_Sim;

import java.sql.*;

public class Modele_p {
	
	Connection c = null;
	Statement statement = null;
	ResultSet resultSet = null;
	
	/*
	 * Fait une recherche dans la BD en donnant le nom de table, des colonnes, et le type de stock
	 * retourne la quantité du stock
	 */
	public int rechercheBD(String table, String colonne1, String colonne2,  String stock) throws SQLException {
		
		int quantite = 0;
		String requeteSQL = "SELECT "+colonne1+" FROM "+table+" WHERE "+colonne2+" = '"+stock+"';";
	
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:ProductionTSF.db");
			statement = c.createStatement();
			//System.out.println("SQLite DB connectée!!!");
		}catch (Exception e) {
			System.out.println("Exception générée par : " + e);
		}
		
		try {
			statement.executeUpdate(requeteSQL);
		} catch (SQLException e) {
			System.out.println("Exception SQL générée par: " + e);
		}
		
		try {
			ResultSet rs = statement.executeQuery(requeteSQL);
			while (rs.next()) {
				quantite = rs.getInt(colonne1);
				}
			} catch (SQLException e) {
				System.out.println("Exception SQL générée par: "+e);
			}
		c.close();
		return quantite;
	}
	
	/*
	 * Fait une requete dans la BD,
	 * retourne la quantite demandé dans la requete
	 */
	public int requeteBD(String requete, String colonne) throws SQLException {
		
		int quantite = 0;
		String requeteSQL = requete;
	
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:ProductionTSF.db");
			statement = c.createStatement();
			//System.out.println("SQLite DB connectée!!!");
		}catch (Exception e) {
			System.out.println("Exception générée par : " + e);
		}
		
		try {
			statement.executeUpdate(requeteSQL);
		} catch (SQLException e) {
			System.out.println("Exception SQL générée par: " + e);
		}
		
		try {
			ResultSet rs = statement.executeQuery(requeteSQL);
			while (rs.next()) {
				quantite = rs.getInt(colonne);
				}
			} catch (SQLException e) {
				System.out.println("Exception SQL générée par: "+e);
			}
		c.close();
		return quantite;
	}
	
	/*
	 * Met à jour la base de donnée,
	 */
	public void updateBD(String table, String colonne1, String colonne2, int stock, String rangee) throws SQLException {
		
		String requeteSQL = "UPDATE "+table+" SET "+colonne1+" = "+stock+" WHERE "+colonne2+" = '"+rangee+"';";

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:ProductionTSF.db");
			statement = c.createStatement();
			//System.out.println("SQLite DB connectée!!!");
		}catch (Exception e) {
			System.out.println("Exception générée par : " + e);
		}
		
		try {
			statement.executeUpdate(requeteSQL);
		} catch (SQLException e) {
			System.out.println("Exception SQL générée par: " + e);
		}
		c.close();
	}
	
	/*
	 * Insère dans la table Ventes les détails des ventes
	 */
	public void insereBD(String date, int table33, int table77, int tablesTotal) throws SQLException {
		
		String requeteSQL = "INSERT INTO Ventes(idJournee, qteTables33, qteTables77, qteTotale) VALUES ('"+date+"', "+table33+", "+table77+", "+tablesTotal+");";
	
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:ProductionTSF.db");
			statement = c.createStatement();
			//System.out.println("SQLite DB connectée!!!");
		}catch (Exception e) {
			System.out.println("Exception générée par : " + e);
		}
		
		try {
			statement.executeUpdate(requeteSQL);
		} catch (SQLException e) {
			System.out.println("Exception SQL générée par: " + e);
		}
		c.close();
	}
	
	/*
	 * Insère dans la BD un nouveau log avec la date, la quantité des stocks et le nombre de ventes
	 */
	public void insereLogBD(String date, int stockA, int stockB, int stockC, int table33, int table77, int ventes33, int ventes77, int ventesTot) throws SQLException {
		
		String requeteSQL = "INSERT INTO Log(idJournee, stockA, stockB, stockC, tables33, tables77, ventes33, ventes77, ventesTotales)"
				+"VALUES ('"+date+"',"+stockA+","+stockB+","+stockC+","+table33+","+table77+","+ventes33+","+ventes77+","+ventesTot+");";
	
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:ProductionTSF.db");
			statement = c.createStatement();
			//System.out.println("SQLite DB connectée!!!");
		}catch (Exception e) {
			System.out.println("Exception générée par : " + e);
		}
		
		try {
			statement.executeUpdate(requeteSQL);
		} catch (SQLException e) {
			System.out.println("Exception SQL générée par: " + e);
		}
		c.close();
	}
	
	/*
	 * Créé une commande en calculant la quantité maximale d'une recette fois 
	 * 4 par heure, 8 heures par jour, 5 jours par semaine fois 2 semaines
	 * retourne la quantité nécéssaire pour deux semaines
	 */
	public int creerCommande(int quantite) {
		return quantite*4*8*5*2;
	}
	
	/*
	 * Organise la date en jour par semaine pour ensuite la retourner
	 */
	public String concatDate(int valeur1, int valeur2) {
		return "Semaine: "+Integer.toString(valeur2)+"; Jour: "+Integer.toString(valeur1);
	}
	
	/*
	 * Simple soustraction de la quantité d'une recette et la quantité du stock
	 */
	public int utiliserStock(int stock, int quantite) {
		return stock-quantite;
	}
	
	/*
	 * Génère des ventes aléatoires pour chaque produit
	 * retourne un nombre de 8 à 22
	 */
	public int venteProduit() {
		return (int) (8 + Math.random() * (22-8));
	}
	
//	public int ventesParJour(int vieilleValeur, int nouvelleValeur) {
//		return (vieilleValeur + nouvelleValeur) / 2;
//	}
}
